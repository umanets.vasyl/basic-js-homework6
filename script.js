"use strict";

function createNewUser() {
  let newUser = {
    firstName: prompt("Enter your first name", "Denys"),
    lastName: prompt("Enter your last name", "Kravtsov"),
    birthday: prompt("Enter your birthday (YYYY-MM-DD)", "2004-12-15"),

    getLogin() {
      return (
        this.firstName.toLowerCase().slice(0, 1) + this.lastName.toLowerCase()
      );
    },

    getFullName() {
      return `${this.firstName} ${this.lastName}`;
    },

    getAge() {
      let today = new Date();
      let birthDate = new Date(this.birthday);
      let age = today.getFullYear() - birthDate.getFullYear();
      let m = today.getMonth() - birthDate.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      return age;
    },

    getPassword() {
      let birthDate = new Date(this.birthday);
      return (
        this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthDate.getFullYear()
      );
    },

    setFirstName(newFirstName) {
      Object.defineProperty(this, "firstName", {
        value: newFirstName,
        writable: true,
      });
    },

    setLastName(newLastName) {
      Object.defineProperty(this, "lastName", {
        value: newLastName,
        writable: true,
      });
    },
  };

  Object.defineProperty(newUser, "firstName", {
    configurable: true,
    writable: false,
  });

  Object.defineProperty(newUser, "lastName", {
    configurable: true,
    writable: false,
  });

  return newUser;
}

let userOne = new createNewUser();

console.log(`User full name is ${userOne.getFullName()}`);
console.log(`User login is ${userOne.getLogin()}`);
console.log(`User age is ${userOne.getAge()}`);
console.log(`User password is ${userOne.getPassword()}`);

userOne.setFirstName("Viktor");
userOne.setLastName("Mykolenko");

console.log(`User full name is ${userOne.getFullName()}`);
console.log(`User login is ${userOne.getLogin()}`);
console.log(`User age is ${userOne.getAge()}`);
console.log(`User password is ${userOne.getPassword()}`);
